################################################################################
## It is corresponsing to the following entry in the On-Line Encyclopedia     ##
## of Integer Sequences (OEIS):                                               ##
## https://oeis.org/search?q=A117686                                          ##
##                                                                            ##
## Author: Mehrad Mahmoudian                                                  ##
## License: GPL3                                                              ##
##                                                                            ##
## Description:                                                               ##
##   The number in the sequence is a valid number if the 
################################################################################


#-------[ load packages ]-------#
{
    library("parallel")
    library("foreach")
    library("doParallel")
}


#-------[ initial settings ]-------#
{
    ## what range of numbers should it cover
    # init_lower_bound <- 10
    init_lower_bound <- 1
    init_upper_bound <- 12101 #1000000000000

    
    # how many tasks should we push to each CPU core (this depends on the processing
    # power of each core and the total available memory, so don't set it too high
    init_jumps <- 450
    
    
    # define how many cores should not be used
    init_free_CPU_cores <- 1
}


#-------[ internal functions ]-------#
{
    func_cutter <- function(x, cuts){
        ## Description:
        ##  A function to cut a string at given points
        ##
        ## Arguments:
        ##  x:     A character vector of length 1
        ##  cuts:  a numeric vector of length smaller than nchar(x)
        
        # create a collective variable
        final <- c()
        
        # go through all the cutting points that is provided to the function
        for(i in seq_along(cuts)){
            
            # if the index is 1
            if(i == 1){
                # append the first few chracters as an item to the collective vector
                final <- c(final, substr(x = x, start = 1, stop = cuts[i]))
            }
            
            # if we got to the end of the cutting points list
            if(i == length(cuts)){
                # set the ending to be the end of the string
                tmp_end_index <- nchar(x)
            }else{
                # using the next cutting point as the ending position
                tmp_end_index <- cuts[i+1]
            }
            
            final <- c(final, substr(x = x, start = cuts[i]+1, stop = tmp_end_index))
        }
        
        return(final)
    }
    
    
    chunk2 <- function(x,n){
        ## Description:
        ##   A function to break the provided vector into n equal smaller vectors
        ##
        ## Arguments:
        ##   x: a vector that you want to cut into chunks
        ##   n: number of chunks
        ##
        ## source: https://stackoverflow.com/a/16275428/1613005
        
        split(x,
              cut(seq_along(x),
                  n,
                  labels = FALSE))
    }
}


#-------[ ininital setup ]-------#
{
    # set the number to maximum possible but leave some out for the grace
    parallel.cores <- parallel::detectCores() - init_free_CPU_cores

    ## register parallel backend
    cl <- parallel::makeCluster(parallel.cores, outfile = "")
    doParallel::registerDoParallel(cl)

}


#-------[ main ]-------#
{
    # create an empty vector to be filled in the loop below
    final <- c()
    
    # initiate the start of i in the loop
    i_low <- init_lower_bound
    
    
    ## initiate the result file with a separator tagged with date and time
    header_txt <- paste0("[ ", format(Sys.time(), "%Y%m%d-%H%M%S"), " ]")
    padding_length <- (80 - nchar(header_txt)) / 2
    header_txt <- paste0(paste(rep.int("-", ceiling(padding_length)), collapse = ""),
                         header_txt,
                         paste(rep.int("-", floor(padding_length)), collapse = ""))
    cat(header_txt, file = "result.txt", sep = "\n", append = T)

    
    
    # run as long as i is less than the upper bound
    while(i_low <= init_upper_bound){
        # define how high we want to go in this round of while
        i_high <- i_low + (parallel.cores * init_jumps)
        
        # if the hight we defined is larger than the user-defined upper bound
        if(i_high > (init_upper_bound + 1)){
            # trim it to be as high as upper bound +1 (+1 is to make the while loop break)
            i_high <- init_upper_bound + 1
        }
        
        # break the distance between i_low to i_high to equal chunks where chunk count is equal to parallel.cores
        chunks <- chunk2(i_low:i_high, parallel.cores)
        
        
        cat(paste0("[", format(Sys.time(), "%Y%m%d-%H%M%S"), "]\t"), i_low, ":", i_high, ":: ")
        
        
        # got through the numbers between the lower and upper limits of this round of while
        # tmp_final <- foreach(ii = chunks,
        #                      .inorder = TRUE,
        #                      .combine = c) %dopar% {
        for(ii in chunks){
                                tmp_outer_collective <- c()
                                
                                for(i in ii){
                                    
                                    tmp_inner_collective <- c()
                                    
                                    # make square
                                    i_sqrd <- i^2
                                    
                                    
                                    # iterate through the possible number of cuts we can have to the string
                                    for(j in seq_len(nchar(i_sqrd) - 1)){
                                        # generate all possible combination of cutting positions considering the
                                        # constraint on total number of cuts allowed
                                        combinations <- apply(combn(x = nchar(i_sqrd) - 1, m = j), 2, function(a){
                                            func_cutter(x = i_sqrd, cuts = a)
                                        })
                                        
                                        
                                        # go through all generated pieces and do the math
                                        for(k in 1:ncol(combinations)){
                                            # select this specific combination
                                            x <- as.character(combinations[, k])
                                            
                                            # turn this combination into numbers
                                            x_numeric <- as.numeric(x)
                                            
                                            # generate the product
                                            x_prod <- prod(x_numeric)
                                            
                                            # remove those that are zero
                                            x_prod <- x_prod[x_prod != 0]
                                            
                                            # calculate the 3rd root
                                            x_prod_3rd_root <- x_prod^(1/3)
                                            
                                            # only select those that are integers
                                            x_prod_3rd_root_valid <- x_prod_3rd_root[floor(x_prod_3rd_root) == x_prod_3rd_root]
                                            
                                            # if the 3rd root is an integer itself
                                            if(length(x_prod_3rd_root_valid)){
                                                # store the decomposition
                                                tmp_success <- paste0("(", paste0(x, collapse = "*"), ")^(1/3) = ", x_prod^(1/3))
                                                
                                                # add to the collective variable
                                                tmp_inner_collective <- c(tmp_inner_collective, tmp_success)
                                            }
                                        }
                                    }
                                   
                                   
                                    # if the collective variable was not empty
                                    if(length(tmp_inner_collective)){
                                        # retusn a character vector of length 1 with correct desired format
                                        tmp_inner_collective <- paste0(paste0("[",
                                                                        format(Sys.time(),
                                                                                "%Y%m%d-%H%M%S"),
                                                                        "]\t"),
                                                                i,
                                                                "\t::\t",
                                                                paste(tmp_inner_collective,
                                                                        collapse = ", "))
                                    }else{
                                        tmp_inner_collective <- NULL
                                    }
                                    
                                    tmp_outer_collective <- c(tmp_outer_collective, tmp_inner_collective)
                                   
                                }
                                
                                
                                return(tmp_outer_collective)
                                 
                             }
        
        
        # append to the global collective1 variable 
        final <- c(final, tmp_final)
        
        if(!is.null(tmp_final)){
            cat(length(tmp_final), "\n")
            cat(tmp_final, file = "result.txt", sep = "\n", append = T)
        }else{
            cat("0\n")
        }
        
        # put the highest I we covered to be the lowest to get ready for the next round of while loop
        i_low <- i_high
        
        invisible(gc())
        
        # keep track of what was the latest (in case it is running in an unstable machine e.g electricity problem or SLURM)
        cat(paste("The latest value checked is:\n",
                  paste0("[",
                         format(Sys.time(),
                                "%Y%m%d-%H%M%S"),
                         "]\t")
                  , i_high),
            file = "latest.txt", 
            sep = "\n", 
            append = F)
    }
    
    parallel::stopCluster(cl)
    
    invisible(gc())
}
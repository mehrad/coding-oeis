# [A117686](https://oeis.org/A117686) - Squares for which the product of the digits are cubes.

Squares for which the product of the digits are cubes.

## EXAMPLE

81 is in the sequence because it is a square and the product of its digits 1*8=8 is a cube.




- 0, 1, 81, 100, 400, 900, 1024, 1089, 1444, 1600, 2025, 2209, 2304, 2401, 2500, 2601, 2704, 2809, 3025, 3249, 3364, 3600, 4096, 4900, 5041, 6084, 6400, 7056, 8100, 8649, 9025, 9409, 9604, 9801, 10000, 10201, 10404, 10609, 10816, 11025, 11881, 12100 (list; graph; refs; listen; history; text; internal format)
- OFFSET: 0,3
- LINKS: Table of n, a(n) for n=0..41.
- EXAMPLE: 81 is in the sequence because it is a square and the product of its digits 1*8=8 is a cube.
- MATHEMATICA: Select[Range[0, 200]^2, IntegerQ[Surd[Times@@IntegerDigits[#], 3]]&] (* Harvey P. Dale, Feb 01 2019 *)
- CROSSREFS: Cf. A000290, A000578.
- Sequence in context: A179444 A068834 A067521 * A104113 A102766 A256349
- Adjacent sequences: A117683 A117684 A117685 * A117687 A117688 A117689
- KEYWORD: nonn,base
- AUTHOR: Luc Stevens (lms022(AT)yahoo.com), Apr 12 2006
- STATUS: approved 

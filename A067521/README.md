# [A067521](https://oeis.org/A067521)

Numbers n such that the square root of n is an integer and a multiple of the sum of the digits of n.", meaning (sqrt(2025) / (2+0+2+5)) is an integer (has no  decimal numbers or in other words has no remainder.

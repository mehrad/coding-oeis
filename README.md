# Coding solutions for OEIS

This repo contains the code and perhaps the results of those codes for the integer series in OEIS (Online Encyclopedia of Integer Sequences). This repo is not affiliated with the OEIS.org and does not claim to have correct codes. The codes in this repo are not guaranteed to be correct or accurate or produce the exact output for each of the integer series. These are the best attempts of the contributors. In case you spot an error, please be kind and report them in the issues section of the repo or even better, fork the repo apply the changes and document them and create a PR (Pull Request).


The following are some useful links:

- [b-file format](https://oeis.org/SubmitB.html)


# [A104113](https://oeis.org/A104113)

Numbers which when chopped into one, two or more parts, added and squared result in the same number.

OFFSET      1,3
LINKS       Table of n, a(n) for n=1..29.
EXAMPLE     a(5)=1296 as (1+29+6)^2 = 36^2 = 1296.
CROSSREFS   Cf. A102766.
            Sequence in context: A068834 A067521 A117686 * A102766 A256349 A202002
            Adjacent sequences:  A104110 A104111 A104112 * A104114 A104115 A104116
KEYWORD     nonn,base
AUTHOR 	    Bodo Zinser, Mar 05 2005
STATUS      approved


